<?php namespace App;

use Laravel\Lumen\Application;

use Error;
use Closure;
use Exception;
use Throwable;
use ErrorException;
use Monolog\Logger;
use RuntimeException;
use FastRoute\Dispatcher;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pipeline\Pipeline;
use Monolog\Handler\StreamHandler;
use Illuminate\Container\Container;
use Illuminate\Foundation\Composer;
use Monolog\Formatter\LineFormatter;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Config\Repository as ConfigRepository;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Contracts\Foundation\Application as ApplicationContract;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

/**
 * @package newshop.dev
 * @author  msh $ 16-3-19 4:28 $
 */
class Core extends Application
{
    public $appPath;

    public function __construct($basePath)
    {
        parent::__construct($basePath);
        $this->storagePath = $this->basePath('data/storage');
    }

    /**
     * Get the database path for the application.
     *
     * @return string
     */
    public function databasePath()
    {
        return $this->path().'/Database';
    }

    /**
     * Get the path to the application "app" directory.
     * @param string $path
     * @return string
     */
    public function path($path = null)
    {
        return $this->basePath.'app/framework'.($path ? '/'.$path : $path);
    }
    
}
